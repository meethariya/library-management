package com.virtusa.microservices.paymentservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.virtusa.microservices.paymentservice.model.Wallet;

/**
 * Repository for {@link Wallet} entity.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public interface WalletRepository extends JpaRepository<Wallet, Integer> {

	/**
	 * Find wallet by User id.
	 * 
	 * @param userId used to fetch wallet.
	 * @return optional of Wallet
	 */
	public Optional<Wallet> findByUserId(int userId);
}
