package com.virtusa.microservices.paymentservice.controller;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservices.paymentservice.dto.RequestTransactionDto;
import com.virtusa.microservices.paymentservice.dto.ResponseTransactionDto;
import com.virtusa.microservices.paymentservice.exception.AccountServiceUnavailableException;
import com.virtusa.microservices.paymentservice.exception.InsufficientBalanceException;
import com.virtusa.microservices.paymentservice.exception.InvalidDetailsException;
import com.virtusa.microservices.paymentservice.exception.TransactionNotFoundException;
import com.virtusa.microservices.paymentservice.exception.WalletNotFoundException;
import com.virtusa.microservices.paymentservice.service.PaymentService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;

/**
 * Controller for Transaction operations.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Slf4j
@RestController
@RequestMapping("/api/payment/transaction")
@CircuitBreaker(name = "getUser", fallbackMethod = "getUserFallback")
public class TransactionController {

	@Autowired
	PaymentService service;

	/**
	 * Create a new transaction.
	 * 
	 * @param request RequestTransactionDto
	 * @return ResponseTransactionDto
	 * @throws WalletNotFoundException
	 */
	@PostMapping
	public ResponseEntity<ResponseTransactionDto> createTransaction(@ModelAttribute RequestTransactionDto request) {
		return new ResponseEntity<>(service.createTransaction(request), HttpStatus.CREATED);
	}

	/**
	 * Get all transactions. <b> For Admin </b>
	 * 
	 * @return List of ResponseTransactionDto
	 */
	@GetMapping("/admin/getAllTransactions")
	public ResponseEntity<List<ResponseTransactionDto>> getTransaction() {
		return new ResponseEntity<>(service.getTransaction(), HttpStatus.OK);
	}

	/**
	 * Get transaction using ID.
	 * 
	 * @param transactionId UUID of transaction
	 * @return ResponseTransactionDto-
	 * @throws TransactionNotFoundException
	 */
	@GetMapping("/{transactionId}")
	public ResponseEntity<ResponseTransactionDto> getTransaction(@PathVariable("transactionId") UUID transactionId) {
		return new ResponseEntity<>(service.getTransaction(transactionId), HttpStatus.OK);
	}

	/**
	 * Get all transactions of a wallet Id.
	 * 
	 * @param walletId fetch using walletId
	 * @return List of ResponseTransactionDto
	 * @throws WalletNotFoundException
	 */
	@GetMapping("/walletId/{walletId}")
	public ResponseEntity<List<ResponseTransactionDto>> getTransaction(@PathVariable("walletId") int walletId) {
		return new ResponseEntity<>(service.getTransaction(walletId), HttpStatus.OK);
	}

	/**
	 * Update Status of the transaction.
	 * 
	 * @param request RequestTransactionDto
	 * @return ResponseTransactionDto
	 * @throws TransactionNotFoundException
	 */
	@PutMapping("/{transactionId}")
	public ResponseEntity<ResponseTransactionDto> updateTransaction(@ModelAttribute RequestTransactionDto request,
			@PathVariable("transactionId") UUID transactionId) {
		return new ResponseEntity<>(service.updateTransaction(request, transactionId), HttpStatus.ACCEPTED);
	}

	/**
	 * Execute Transaction.
	 * 
	 * @param transactionId the transaction to work on.
	 * @return ResponseTransactionDto
	 * @throws InsufficientBalanceException
	 * @throws InvalidDetailsException
	 */
	@GetMapping("/execute/{transactionId}")
	public ResponseEntity<ResponseTransactionDto> executeTransaction(
			@PathVariable("transactionId") UUID transactionId) {
		return new ResponseEntity<>(service.executeTransaction(transactionId), HttpStatus.ACCEPTED);
	}

	/**
	 * Download Transaction invoice.
	 * 
	 * @param transactionId
	 * @return invoice file
	 * @throws IOException
	 */
	@GetMapping("/transactionInvoice/{transactionId}")
	@CircuitBreaker(name = "getUser", fallbackMethod = "generateInvoiceFallback")
	public ResponseEntity<Resource> generateTransactionInvoice(@PathVariable("transactionId") UUID transactionId)
			throws IOException {
		Resource resource = service.generateTransactionInvoice(transactionId);
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=transaction_invoice_" + transactionId + ".pdf");
		return ResponseEntity.ok().headers(headers).contentLength(resource.contentLength())
				.contentType(MediaType.APPLICATION_PDF).body(resource);
	}

	/**
	 * Fallback method for {@link #getUser(int)}.
	 * 
	 * @param userId used to fetch user.
	 * @param e      exception
	 * @return null
	 */
	public ResponseEntity<ResponseTransactionDto> getUserFallback(RequestTransactionDto request, Exception e) {
		throwAccountServiceUnavailableException(e);
		return null;
	}

	/**
	 * Fallback method for {@link #getUser(int)}.
	 * 
	 * @param userId used to fetch user.
	 * @param e      exception
	 * @return null
	 */
	public ResponseEntity<List<ResponseTransactionDto>> getUserFallback(Exception e) {
		throwAccountServiceUnavailableException(e);
		return null;
	}

	/**
	 * Fallback method for {@link #getUser(int)}.
	 * 
	 * @param userId used to fetch user.
	 * @param e      exception
	 * @return null
	 */
	public ResponseEntity<List<ResponseTransactionDto>> getUserFallback(int walletId, Exception e) {
		throwAccountServiceUnavailableException(e);
		return null;
	}
	
	/**
	 * Fallback method for {@link #getUser(int)}.
	 * 
	 * @param userId used to fetch user.
	 * @param e      exception
	 * @return null
	 */
	public ResponseEntity<Resource> generateInvoiceFallback(UUID transactionId, Exception e) throws IOException{
		throwAccountServiceUnavailableException(e);
		return null;
	}

	/**
	 * Fallback method for {@link #getUser(int)}.
	 * 
	 * @param userId used to fetch user.
	 * @param e      exception
	 * @return null
	 */
	public ResponseEntity<ResponseTransactionDto> getUserFallback(UUID transactionId, Exception e) {
		throwAccountServiceUnavailableException(e);
		return null;
	}

	/**
	 * Fallback method for {@link #getUser(int)}.
	 * 
	 * @param userId used to fetch user.
	 * @param e      exception
	 * @return null
	 */
	public ResponseEntity<ResponseTransactionDto> getUserFallback(RequestTransactionDto request, UUID transactionId,
			Exception e) {
		throwAccountServiceUnavailableException(e);
		return null;
	}

	/**
	 * Logs message and throws {@link AccountServiceUnavailableException}
	 * 
	 * @param e
	 * @throws AccountServiceUnavailableException
	 */
	private void throwAccountServiceUnavailableException(Exception e) {
		log.error(e.getMessage());
		throw new AccountServiceUnavailableException();
	}
}
