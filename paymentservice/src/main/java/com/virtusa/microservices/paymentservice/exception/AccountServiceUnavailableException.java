package com.virtusa.microservices.paymentservice.exception;

/**
 * Throw this <b>RuntimeException</b> when account service is unavailable.
 * 
 * @author MEETKIRTIBHAI
 * @since 13-Oct-2023
 */
public class AccountServiceUnavailableException extends RuntimeException {

	/**
	 * Default generated.
	 */
	private static final long serialVersionUID = 1112071486786904910L;

	/**
	 * Default Constructor
	 */
	public AccountServiceUnavailableException() {
		super("Account service is currently unavailable");
	}

	/**
	 * Constructor with error message.
	 * 
	 * @param message error message
	 */
	public AccountServiceUnavailableException(String message) {
		super(message);
	}

}
