package com.virtusa.microservices.paymentservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservices.paymentservice.dto.RequestWalletDto;
import com.virtusa.microservices.paymentservice.dto.ResponseCreateWalletDto;
import com.virtusa.microservices.paymentservice.dto.ResponseWalletDto;
import com.virtusa.microservices.paymentservice.exception.AccountServiceUnavailableException;
import com.virtusa.microservices.paymentservice.exception.InvalidDetailsException;
import com.virtusa.microservices.paymentservice.exception.WalletNotFoundException;
import com.virtusa.microservices.paymentservice.service.PaymentService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;

/**
 * Controller for Wallet operations.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Slf4j
@RestController
@RequestMapping("/api/payment/wallet")
@CircuitBreaker(name = "getUser", fallbackMethod = "getUserFallback")
public class WalletController {

	@Autowired
	PaymentService service;

	/**
	 * Create wallet for a user.
	 * 
	 * @param request RequestWalletDto
	 * @return ResponseWalletDto
	 * @throws InvalidDetailsException
	 */
	@PostMapping
	public ResponseEntity<ResponseCreateWalletDto> createWallet(@ModelAttribute RequestWalletDto request) {
		return new ResponseEntity<>(service.createWallet(request), HttpStatus.CREATED);
	}

	/**
	 * Get Wallet using id.
	 * 
	 * @param walletId fetch using id
	 * @return ResponseWalletDto
	 * @throws WalletNotFoundException
	 */
	@GetMapping("/{walletId}")
	public ResponseEntity<ResponseWalletDto> getWallet(@PathVariable("walletId") int walletId) {
		return new ResponseEntity<>(service.getWallet(walletId), HttpStatus.OK);
	}

	/**
	 * Get Wallet using user id.
	 * 
	 * @param userId fetch using user id
	 * @return ResponseWalletDto
	 * @throws WalletNotFoundException
	 */
	@GetMapping("/userId/{userId}")
	public ResponseEntity<ResponseWalletDto> getWalletByUserId(@PathVariable("userId") int userId) {
		return new ResponseEntity<>(service.getWalletByUserId(userId), HttpStatus.OK);
	}

	/**
	 * Update wallet's balance.
	 * 
	 * @param request  RequestWalletDto
	 * @param walletId fetch using id
	 * @return ResponseWalletDto
	 * @throws WalletNotFoundException
	 * @throws InvalidDetailsException
	 */
	@PutMapping("/{walletId}")
	public ResponseEntity<ResponseWalletDto> updateWallet(@ModelAttribute RequestWalletDto request,
			@PathVariable("walletId") int walletId) {
		return new ResponseEntity<>(service.updateWallet(request, walletId), HttpStatus.ACCEPTED);
	}

	/**
	 * Delete wallet.
	 * 
	 * @param walletId fetch using id
	 */
	@DeleteMapping("/{walletId}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void deleteWallet(@PathVariable("walletId") int walletId) {
		service.deleteWallet(walletId);
	}

	/**
	 * Delete wallet.
	 * 
	 * @param userId fetch using user id
	 * @throws WalletNotFoundException
	 */
	@DeleteMapping("/userId/{userId}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void deleteWalletByUserId(@PathVariable("userId") int userId) {
		service.deleteWalletByUserId(userId);
	}

	/**
	 * Fallback method for {@link #getUser(int)}.
	 * 
	 * @param userId used to fetch user.
	 * @param e      exception
	 * @return null
	 */
	public ResponseEntity<ResponseWalletDto> getUserFallback(int userId, Exception e) {
		log.error(e.getMessage());
		throw new AccountServiceUnavailableException();
	}

	/**
	 * Fallback method for {@link #getUser(int)}.
	 * 
	 * @param userId used to fetch user.
	 * @param e      exception
	 * @return null
	 */
	public ResponseEntity<ResponseWalletDto> getUserFallback(RequestWalletDto request, int walletId, Exception e) {
		log.error(e.getMessage());
		log.error(request.getUserId() + " : " + walletId);
		throw new AccountServiceUnavailableException();
	}
}
