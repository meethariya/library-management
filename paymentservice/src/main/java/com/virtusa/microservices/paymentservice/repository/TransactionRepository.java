package com.virtusa.microservices.paymentservice.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.virtusa.microservices.paymentservice.model.Transaction;
import com.virtusa.microservices.paymentservice.model.Wallet;

/**
 * Repository for {@link Transaction} entity.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public interface TransactionRepository extends JpaRepository<Transaction, UUID> {

	/**
	 * Find all transaction where user has sent money.
	 * 
	 * @param sender wallet
	 * @return List of transactions
	 */
	public List<Transaction> findBySender(Wallet sender);
}
