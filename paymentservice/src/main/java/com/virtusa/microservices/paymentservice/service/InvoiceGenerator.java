package com.virtusa.microservices.paymentservice.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.virtusa.microservices.paymentservice.exception.InvoiceGenerationException;
import com.virtusa.microservices.paymentservice.model.Transaction;

import lombok.extern.slf4j.Slf4j;

/**
 * Generate Invoices.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Component
@Slf4j
public class InvoiceGenerator {

	@Value("${transaction.invoice.path}")
	private String transactionPath;

	/**
	 * Generate invoice for the transaction.
	 * 
	 * @param transaction
	 * @param username
	 * @return path of the invoice.
	 * @throws IOException
	 * @throws InvoiceGenerationException
	 */
	public String generateTransactionInvoice(Transaction transaction, String username) throws IOException {
		// Create Directory if dosent exists
		Files.createDirectories(Paths.get(transactionPath));

		String destination = transactionPath + transaction.getTransactionId() + ".pdf";

		// creating pdf components
		Document document = new Document();
		document.addCreationDate();

		// Fonts
		Font f1 = new Font();
		f1.setStyle(Font.BOLD);
		f1.setSize(18);
		Font f2 = new Font();
		f2.setSize(12);

		try {
			PdfWriter.getInstance(document, new FileOutputStream(new File(destination)));
			document.open();
			// Company name
			document.add(createParagraph("Library Management\n", Element.ALIGN_CENTER, f1));
			// Left Content
			document.add(createParagraph("A2,Raja Road\nDelhi-294994\n\nInvoice Date: " + transaction.getTime()
					+ "\nBill To: " + username + "\n\nPayment Method: WALLET\n\nAmount: Rs." + transaction.getAmount()
					+ "\n\nThank you for your business!\n\nAdmin\nadmin@gmail.com", Element.ALIGN_LEFT, f2));
			document.close();
			return destination;
		} catch (FileNotFoundException | DocumentException e) {
			log.error(e.getMessage());
			throw new InvoiceGenerationException(e.getMessage());
		}
	}

	/**
	 * Creates new Paragraph for given text.
	 * 
	 * @param text      content of paragraph
	 * @param alignment paragrpah alignment
	 * @param f         font
	 * @return
	 */
	private Paragraph createParagraph(String text, int alignment, Font f) {
		Paragraph temp = new Paragraph(text);
		temp.setAlignment(alignment);
		temp.setFont(f);
		return temp;
	}
}
