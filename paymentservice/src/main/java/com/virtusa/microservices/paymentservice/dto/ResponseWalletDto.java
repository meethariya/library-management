package com.virtusa.microservices.paymentservice.dto;

import java.util.Date;

import com.virtusa.microservices.paymentservice.model.Wallet;

import lombok.Data;

/**
 * Response DTO to be given back to end-user for {@link Wallet}.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Data
public class ResponseWalletDto {

	private int id;

	private ResponseUserDto user;

	private double balance;

	private Date lastUpdated;
}
