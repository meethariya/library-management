package com.virtusa.microservices.paymentservice.exception;

/**
 * Throw this <b>RuntimeException</b> when there is error generating invoice.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public class InvoiceGenerationException extends RuntimeException {

	/**
	 * Default generated
	 */
	private static final long serialVersionUID = 5626387471157137964L;

	/**
	 * Default constructor
	 */
	public InvoiceGenerationException() {
		super();
	}

	/**
	 * Constructor with message.
	 * 
	 * @param message
	 */
	public InvoiceGenerationException(String message) {
		super(message);
	}

}
