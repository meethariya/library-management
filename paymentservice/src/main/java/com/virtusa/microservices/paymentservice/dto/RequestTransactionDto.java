package com.virtusa.microservices.paymentservice.dto;

import com.virtusa.microservices.paymentservice.model.Transaction;

import lombok.Data;

/**
 * Request DTO taken from end-user for {@link Transaction}.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Data
public class RequestTransactionDto {

	private int senderUserId;

	private int receiverUserId;

	private boolean status;

	private double amount;
}
