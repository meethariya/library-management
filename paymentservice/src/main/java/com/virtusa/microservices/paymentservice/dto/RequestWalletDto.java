package com.virtusa.microservices.paymentservice.dto;

import com.virtusa.microservices.paymentservice.model.Wallet;

import lombok.Data;

/**
 * Request DTO taken from end-user for {@link Wallet}.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Data
public class RequestWalletDto {
	private int userId;
	private double amount;
}
