package com.virtusa.microservices.paymentservice.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Capability;
import feign.micrometer.MicrometerCapability;
import io.micrometer.core.instrument.MeterRegistry;

/**
 * Configuration for all additional beans.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Configuration
public class AdditionalBeanConfig {
	/**
	 * Bean for {@link ModelMapper}.
	 * 
	 * @return new object for ModelMapper
	 */
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	/**
	 * Bean for Micrometer Capability
	 * 
	 * @param registry
	 * @return Micrometer Capability
	 */
	@Bean
	public Capability capability(final MeterRegistry registry) {
		return new MicrometerCapability(registry);
	}

}
