package com.virtusa.microservices.paymentservice.exception;

import com.virtusa.microservices.paymentservice.model.Transaction;

/**
 * Throw this <b>RuntimeException</b> when {@link Transaction} is not found.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public class TransactionNotFoundException extends RuntimeException {

	/**
	 * Default generated
	 */
	private static final long serialVersionUID = -109726648656471020L;

	/**
	 * Default Generated
	 */
	public TransactionNotFoundException() {
		super();
	}

	/**
	 * Constructor with error message.
	 * 
	 * @param message error message.
	 */
	public TransactionNotFoundException(String message) {
		super(message);
	}

}
