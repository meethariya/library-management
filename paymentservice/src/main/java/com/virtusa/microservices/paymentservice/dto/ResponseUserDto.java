package com.virtusa.microservices.paymentservice.dto;

import lombok.Data;

/**
 * Response DTO to be given back to end-user.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Data
public class ResponseUserDto {
	private int id;
	private String role;
	private boolean enabled;
	private String email;
	private String name;
}
