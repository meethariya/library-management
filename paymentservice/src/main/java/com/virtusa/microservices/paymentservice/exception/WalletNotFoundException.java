package com.virtusa.microservices.paymentservice.exception;

import com.virtusa.microservices.paymentservice.model.Wallet;

/**
 * Throw this <b>RuntimeException</b> when {@link Wallet} is not found.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public class WalletNotFoundException extends RuntimeException {

	/**
	 * Default generated
	 */
	private static final long serialVersionUID = 6465235374207641357L;

	/**
	 * Default Constructor
	 */
	public WalletNotFoundException() {
		super();
	}

	/**
	 * Constructor with message.
	 * 
	 * @param message error message
	 */
	public WalletNotFoundException(String message) {
		super(message);
	}

}
