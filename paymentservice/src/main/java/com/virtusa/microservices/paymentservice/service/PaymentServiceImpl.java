package com.virtusa.microservices.paymentservice.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.virtusa.microservices.paymentservice.dto.RequestTransactionDto;
import com.virtusa.microservices.paymentservice.dto.RequestWalletDto;
import com.virtusa.microservices.paymentservice.dto.ResponseCreateWalletDto;
import com.virtusa.microservices.paymentservice.dto.ResponseTransactionDto;
import com.virtusa.microservices.paymentservice.dto.ResponseUserDto;
import com.virtusa.microservices.paymentservice.dto.ResponseWalletDto;
import com.virtusa.microservices.paymentservice.exception.InsufficientBalanceException;
import com.virtusa.microservices.paymentservice.exception.InvalidDetailsException;
import com.virtusa.microservices.paymentservice.exception.TransactionNotFoundException;
import com.virtusa.microservices.paymentservice.exception.WalletNotFoundException;
import com.virtusa.microservices.paymentservice.model.Transaction;
import com.virtusa.microservices.paymentservice.model.Wallet;
import com.virtusa.microservices.paymentservice.openfeign.AccountServiceClient;
import com.virtusa.microservices.paymentservice.repository.TransactionRepository;
import com.virtusa.microservices.paymentservice.repository.WalletRepository;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

/**
 * Implementation for {@link PaymentService}.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Service
@Transactional
@Slf4j
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private ModelMapper modelMapper;

	@Value("${server.port}")
	private int port;

	@Value("${spring.application.name}")
	private String appName;

	@Autowired
	private WalletRepository walletRepository;

	@Autowired
	private TransactionRepository transactionRepository;

	@Autowired
	private AccountServiceClient accountClient;

	@Autowired
	private InvoiceGenerator invoiceGenerator;

	/**
	 * Create wallet for a user.
	 * 
	 * @param request RequestWalletDto
	 * @return ResponseWalletDto
	 * @throws InvalidDetailsException
	 */
	@Override
	public ResponseCreateWalletDto createWallet(RequestWalletDto request) {
		log.info(appName + "@" + port + ": Create Wallet");

		// Check if user already has a wallet.
		try {
			getWalletByUserId(request.getUserId());
			throw new InvalidDetailsException("This user already has a wallet");
		} catch (WalletNotFoundException e) {
			Wallet wallet = requestToWallet(request);
			return modelMapper.map(walletRepository.save(wallet), ResponseCreateWalletDto.class);
		}
	}

	/**
	 * Get Wallet using id.
	 * 
	 * @param walletId fetch using id
	 * @return ResponseWalletDto
	 * @throws WalletNotFoundException
	 */
	@Override
	public ResponseWalletDto getWallet(int walletId) {
		log.info(appName + "@" + port + ": Get Wallet by ID");

		return walletToResponse(getWalletFromRepo(walletId));
	}

	/**
	 * Get Wallet using user id.
	 * 
	 * @param userId fetch using user id
	 * @return ResponseWalletDto
	 * @throws WalletNotFoundException
	 */
	@Override
	public ResponseWalletDto getWalletByUserId(int userId) {
		log.info(appName + "@" + port + ": Get Wallet by user ID");

		return walletToResponse(getWalletByUserIdFromRepo(userId));
	}

	/**
	 * Update wallet's balance.
	 * 
	 * @param request  RequestWalletDto
	 * @param walletId fetch using id
	 * @return ResponseWalletDto
	 * @throws WalletNotFoundException
	 * @throws InvalidDetailsException
	 */
	@Override
	public ResponseWalletDto updateWallet(RequestWalletDto request, int walletId) {
		log.info(appName + "@" + port + ": Update Wallet");

		Wallet wallet = getWalletFromRepo(walletId);

		// can not allow to change user id.
		if (wallet.getUserId() != request.getUserId())
			throw new InvalidDetailsException("Wallet's user can not be changed.");

		wallet.setBalance(wallet.getBalance() + request.getAmount());

		return walletToResponse(walletRepository.save(wallet));
	}

	/**
	 * Delete wallet.
	 * 
	 * @param walletId fetch using id
	 */
	@Override
	public void deleteWallet(int walletId) {
		log.info(appName + "@" + port + ": Delete Wallet by ID");

		walletRepository.deleteById(walletId);
	}

	/**
	 * Delete wallet.
	 * 
	 * @param userId fetch using user id
	 * @throws WalletNotFoundException
	 */
	@Override
	public void deleteWalletByUserId(int userId) {
		log.info(appName + "@" + port + ": Delete Wallet by user ID");

		walletRepository.delete(getWalletByUserIdFromRepo(userId));
	}

	/**
	 * Create a new transaction.
	 * 
	 * @param request RequestTransactionDto
	 * @return ResponseTransactionDto
	 * @throws WalletNotFoundException
	 */
	@Override
	public ResponseTransactionDto createTransaction(RequestTransactionDto request) {
		log.info(appName + "@" + port + ": Create Transaction");

		Transaction transaction = requestToTransaction(request);
		return transactionToResponse(transactionRepository.save(transaction));
	}

	/**
	 * Get all transactions.
	 * 
	 * @return List of ResponseTransactionDto
	 */
	@Override
	public List<ResponseTransactionDto> getTransaction() {
		log.info(appName + "@" + port + ": Get Transaction");

		return transactionRepository.findAll().stream().map(this::transactionToResponse).toList();
	}

	/**
	 * Get transaction using ID.
	 * 
	 * @param transactionId UUID of transaction
	 * @return ResponseTransactionDto
	 * @throws TransactionNotFoundException
	 */
	@Override
	public ResponseTransactionDto getTransaction(UUID transactionId) {
		log.info(appName + "@" + port + ": Get Transaction by ID");
		return transactionToResponse(getTransactionFromRepo(transactionId));
	}

	/**
	 * Get all transactions of a wallet Id.
	 * 
	 * @param walletId fetch using walletId
	 * @return List of ResponseTransactionDto
	 * @throws WalletNotFoundException
	 */
	@Override
	public List<ResponseTransactionDto> getTransaction(int walletId) {
		log.info(appName + "@" + port + ": Create Transaction by Wallet ID");
		List<Transaction> sender = transactionRepository.findBySender(getWalletFromRepo(walletId));
		return sender.stream().map(this::transactionToResponse).toList();
	}

	/**
	 * Update Status of the transaction.
	 * 
	 * @param request RequestTransactionDto
	 * @return ResponseTransactionDto
	 * @throws TransactionNotFoundException
	 */
	@Override
	public ResponseTransactionDto updateTransaction(RequestTransactionDto request, UUID transactionId) {
		log.info(appName + "@" + port + ": Update Transaction");

		Transaction transaction = getTransactionFromRepo(transactionId);
		transaction.setStatus(request.isStatus());
		return transactionToResponse(transactionRepository.save(transaction));
	}

	/**
	 * Execute Transaction.
	 * 
	 * @param transactionId the transaction to work on.
	 * @return ResponseTransactionDto
	 * @throws InvalidDetailsException
	 * @throws InsufficientBalanceException
	 */
	@Override
	public ResponseTransactionDto executeTransaction(UUID transactionId) {
		log.info(appName + "@" + port + ": Execute Transaction");

		Transaction transaction = getTransactionFromRepo(transactionId);

		// validating transaction's state
		if (transaction.isStatus())
			throw new InvalidDetailsException("Transaction already completed");

		Wallet sender = transaction.getSender();
		Wallet receiver = transaction.getReceiver();

		// check balance of sender
		if (sender.getBalance() < transaction.getAmount()) {
			transaction.setStatus(false);
			transactionRepository.save(transaction);
			throw new InsufficientBalanceException("Insufficient balance to perform this transaction.");
		}

		// execute safely
		swapAmount(sender, receiver, transaction.getAmount());

		// update status
		transaction.setStatus(true);
		return transactionToResponse(transactionRepository.save(transaction));
	}

	/**
	 * Download invoice for the transaction.
	 * 
	 * @param transactionId
	 * @return Invoice file
	 * @throws IOException
	 */
	@Override
	public Resource generateTransactionInvoice(UUID transactionId) throws IOException {
		Transaction transaction = getTransactionFromRepo(transactionId);

		// generate invoice
		String destination = invoiceGenerator.generateTransactionInvoice(transaction,
				getUser(transaction.getSender().getUserId()).getName());
		Path path = Paths.get(destination);
		return new ByteArrayResource(Files.readAllBytes(path));

	}

	/**
	 * Separate transactional method to swap amount between sender and receiver.
	 * 
	 * @param sender
	 * @param receiver
	 * @param amount
	 */
	private void swapAmount(Wallet sender, Wallet receiver, double amount) {
		sender.setBalance(sender.getBalance() - amount);
		receiver.setBalance(receiver.getBalance() + amount);
		walletRepository.save(sender);
		walletRepository.save(receiver);
	}

	/**
	 * Get user by id.
	 * 
	 * @param userId used to fetch user.
	 * @return ResponseUserDto
	 */
	private ResponseUserDto getUser(int userId) {
		return accountClient.getUser(userId);
	}

	/**
	 * Get wallet from {@link WalletRepository}.
	 * 
	 * @param walletId used to fetch wallet.
	 * @return wallet entity
	 * @throws WalletNotFoundException
	 */
	private Wallet getWalletFromRepo(int walletId) {
		return walletRepository.findById(walletId)
				.orElseThrow(() -> new WalletNotFoundException("Wallet not found with ID: " + walletId));
	}

	/**
	 * Get wallet from {@link WalletRepository}.
	 * 
	 * @param userId used to fetch wallet.
	 * @return wallet entity
	 * @throws WalletNotFoundException
	 */
	private Wallet getWalletByUserIdFromRepo(int userId) {
		return walletRepository.findByUserId(userId)
				.orElseThrow(() -> new WalletNotFoundException("Wallet not found for user ID: " + userId));
	}

	/**
	 * Converts RequestWalletDto to Wallet entity.
	 * 
	 * @param request RequestWalletDto
	 * @return Wallet entity
	 */
	private Wallet requestToWallet(RequestWalletDto request) {
		Wallet wallet = modelMapper.map(request, Wallet.class);
		wallet.setBalance(wallet.getBalance() + request.getAmount());
		return wallet;
	}

	/**
	 * Converts Wallet entity to ResponseWalletDto.
	 * 
	 * @param wallet
	 * @return ResponseWalletDto
	 */
	private ResponseWalletDto walletToResponse(Wallet wallet) {
		ResponseWalletDto dto = modelMapper.map(wallet, ResponseWalletDto.class);
		dto.setUser(getUser(wallet.getUserId()));
		return dto;
	}

	/**
	 * Converts RequestTransactionDto to Transaction entity.
	 * 
	 * @param request RequestTransactionDto
	 * @return Transaction entitu
	 * @throws WalletNotFoundException
	 */
	private Transaction requestToTransaction(RequestTransactionDto request) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Transaction transaction = modelMapper.map(request, Transaction.class);
		transaction.setSender(getWalletByUserIdFromRepo(request.getSenderUserId()));
		transaction.setReceiver(getWalletByUserIdFromRepo(request.getReceiverUserId()));
		return transaction;
	}

	/**
	 * Converts Transaction entity to ResponseTransactionDto.
	 * 
	 * @param transaction entity
	 * @return ResponseTransactionDto
	 */
	private ResponseTransactionDto transactionToResponse(Transaction transaction) {
		ResponseTransactionDto dto = modelMapper.map(transaction, ResponseTransactionDto.class);
		dto.setSender(walletToResponse(transaction.getSender()));
		dto.setReceiver(walletToResponse(transaction.getReceiver()));
		return dto;
	}

	/**
	 * Get transaction from repository.
	 * 
	 * @param transactionId used to fetch Transaction
	 * @return Transaction
	 * @throws TransactionNotFoundException
	 */
	private Transaction getTransactionFromRepo(UUID transactionId) {
		return transactionRepository.findById(transactionId)
				.orElseThrow(() -> new TransactionNotFoundException("No transaction found with id: " + transactionId));
	}

}
