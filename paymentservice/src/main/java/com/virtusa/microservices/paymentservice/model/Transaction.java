package com.virtusa.microservices.paymentservice.model;

import java.util.Date;
import java.util.UUID;

import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.UuidGenerator;
import org.hibernate.annotations.UuidGenerator.Style;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;

/**
 * Transcation entity for the all records.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Entity
@Data
public class Transaction {
	@Id
	@UuidGenerator(style = Style.RANDOM)
	private UUID transactionId;

	@Column(nullable = true)
	private boolean status;

	@ManyToOne
	@JoinColumn(nullable = false)
	private Wallet sender;

	@ManyToOne
	@JoinColumn(nullable = false)
	private Wallet receiver;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date time;

	@Column(nullable = false)
	private double amount;
}
