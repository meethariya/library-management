package com.virtusa.microservices.paymentservice.service;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.springframework.core.io.Resource;

import com.virtusa.microservices.paymentservice.dto.RequestTransactionDto;
import com.virtusa.microservices.paymentservice.dto.RequestWalletDto;
import com.virtusa.microservices.paymentservice.dto.ResponseCreateWalletDto;
import com.virtusa.microservices.paymentservice.dto.ResponseTransactionDto;
import com.virtusa.microservices.paymentservice.dto.ResponseWalletDto;

/**
 * Service layer for Payment operations.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public interface PaymentService {
	/**
	 * Create wallet for a user.
	 * 
	 * @param request RequestWalletDto
	 * @return ResponseWalletDto
	 */
	public ResponseCreateWalletDto createWallet(RequestWalletDto request);

	/**
	 * Get Wallet using id.
	 * 
	 * @param walletId fetch using id
	 * @return ResponseWalletDto
	 */
	public ResponseWalletDto getWallet(int walletId);

	/**
	 * Get Wallet using user id.
	 * 
	 * @param userId fetch using user id
	 * @return ResponseWalletDto
	 */
	public ResponseWalletDto getWalletByUserId(int userId);

	/**
	 * Update wallet's balance.
	 * 
	 * @param request  RequestWalletDto
	 * @param walletId fetch using id
	 * @return ResponseWalletDto
	 */
	public ResponseWalletDto updateWallet(RequestWalletDto request, int walletId);

	/**
	 * Delete wallet.
	 * 
	 * @param walletId fetch using id
	 */
	public void deleteWallet(int walletId);

	/**
	 * Delete wallet.
	 * 
	 * @param userId fetch using user id
	 */
	public void deleteWalletByUserId(int userId);

	/**
	 * Create a new transaction.
	 * 
	 * @param request RequestTransactionDto
	 * @return ResponseTransactionDto
	 */
	public ResponseTransactionDto createTransaction(RequestTransactionDto request);

	/**
	 * Get all transactions.
	 * 
	 * @return List of ResponseTransactionDto
	 */
	public List<ResponseTransactionDto> getTransaction();

	/**
	 * Get transaction using ID.
	 * 
	 * @param transactionId UUID of transaction
	 * @return ResponseTransactionDto
	 */
	public ResponseTransactionDto getTransaction(UUID transactionId);

	/**
	 * Get all transactions of a wallet Id.
	 * 
	 * @param walletId fetch using walletId
	 * @return List of ResponseTransactionDto
	 */
	public List<ResponseTransactionDto> getTransaction(int walletId);

	/**
	 * Update Status of the transaction.
	 * 
	 * @param request RequestTransactionDto
	 * @return ResponseTransactionDto
	 */
	public ResponseTransactionDto updateTransaction(RequestTransactionDto request, UUID transactionId);

	/**
	 * Execute Transaction.
	 * 
	 * @param transactionId the transaction to work on.
	 * @return ResponseTransactionDto
	 */
	public ResponseTransactionDto executeTransaction(UUID transactionId);

	/**
	 * Download invoice for the transaction.
	 * 
	 * @param transactionId
	 * @return Invoice file
	 * @throws IOException
	 */
	public Resource generateTransactionInvoice(UUID transactionId) throws IOException;
}
