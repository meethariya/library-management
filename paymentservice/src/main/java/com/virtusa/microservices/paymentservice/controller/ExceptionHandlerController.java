package com.virtusa.microservices.paymentservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.virtusa.microservices.paymentservice.exception.AccountServiceUnavailableException;
import com.virtusa.microservices.paymentservice.exception.InsufficientBalanceException;
import com.virtusa.microservices.paymentservice.exception.InvalidDetailsException;
import com.virtusa.microservices.paymentservice.exception.TransactionNotFoundException;
import com.virtusa.microservices.paymentservice.exception.WalletNotFoundException;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller to handle all errors.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@RestControllerAdvice
@Slf4j
public class ExceptionHandlerController {

	/**
	 * Handles error for {@link InsufficientBalanceException}.
	 * 
	 * @param e Error
	 * @return Error message with Not Found status.
	 */
	@ExceptionHandler(InsufficientBalanceException.class)
	public ResponseEntity<String> handleInsufficientBalanceException(InsufficientBalanceException e) {
		log.error(e.getMessage());
		return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
	}

	/**
	 * Handles error for {@link InvalidDetailsException}.
	 * 
	 * @param e Error
	 * @return Error message with Not Found status.
	 */
	@ExceptionHandler(InvalidDetailsException.class)
	public ResponseEntity<String> handleInvalidDetailsException(InvalidDetailsException e) {
		log.error(e.getMessage());
		return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
	}

	/**
	 * Handles error for {@link TransactionNotFoundException}.
	 * 
	 * @param e Error
	 * @return Error message with Not Found status.
	 */
	@ExceptionHandler(TransactionNotFoundException.class)
	public ResponseEntity<String> handleTransactionNotFoundException(TransactionNotFoundException e) {
		log.error(e.getMessage());
		return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
	}

	/**
	 * Handles error for {@link WalletNotFoundException}.
	 * 
	 * @param e Error
	 * @return Error message with Not Found status.
	 */
	@ExceptionHandler(WalletNotFoundException.class)
	public ResponseEntity<String> handleWalletNotFoundException(WalletNotFoundException e) {
		log.error(e.getMessage());
		return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
	}
	
	/**
	 * Handles error for {@link AccountServiceUnavailableException}.
	 * 
	 * @param e Error
	 * @return Error message with service unavailable status.
	 */
	@ExceptionHandler(AccountServiceUnavailableException.class)
	public ResponseEntity<String> handleAccountServiceUnavailableException(AccountServiceUnavailableException e) {
		log.error(e.getMessage());
		return new ResponseEntity<>(e.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
	}

	/**
	 * Handles error for {@link Exception}.
	 * 
	 * @param e Error
	 * @return Error message with Not Found status.
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> handleException(Exception e) {
		log.error(e.getMessage(), e);
		return new ResponseEntity<>("Payment Service is currently unavailable, Try again later",
				HttpStatus.SERVICE_UNAVAILABLE);
	}
}
