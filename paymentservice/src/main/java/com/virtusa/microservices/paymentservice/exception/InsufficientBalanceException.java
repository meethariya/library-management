package com.virtusa.microservices.paymentservice.exception;

import com.virtusa.microservices.paymentservice.model.Wallet;

/**
 * Throw this <b>RuntimeException</b> when there is not enough balance in
 * {@link Wallet}.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public class InsufficientBalanceException extends RuntimeException {

	/**
	 * Default generated
	 */
	private static final long serialVersionUID = 5322335882582797475L;

	/**
	 * Default constructor
	 */
	public InsufficientBalanceException() {
		super();
	}

	/**
	 * Constructor with message.
	 * 
	 * @param message error message
	 */
	public InsufficientBalanceException(String message) {
		super(message);
	}

}
