package com.virtusa.microservices.paymentservice.exception;

/**
 * Throw this <b>RuntimeException</b> when details for any DTO is not accurate.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public class InvalidDetailsException extends RuntimeException {

	/**
	 * Default generated
	 */
	private static final long serialVersionUID = 4968749417129596448L;

	/**
	 * Default Constructor
	 */
	public InvalidDetailsException() {
		super();
	}

	/**
	 * Constructor with error message
	 * 
	 * @param message error message
	 */
	public InvalidDetailsException(String message) {
		super(message);
	}

}
