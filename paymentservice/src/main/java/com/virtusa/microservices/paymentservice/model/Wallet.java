package com.virtusa.microservices.paymentservice.model;

import java.util.Date;

import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;

/**
 * Wallet entity for the person paying/receiving.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Entity
@Data
public class Wallet {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(unique = true, nullable = false)
	private int userId;

	@Column(nullable = false)
	private double balance;

	@UpdateTimestamp
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date lastUpdated;
}
