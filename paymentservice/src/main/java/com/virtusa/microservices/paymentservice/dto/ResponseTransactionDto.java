package com.virtusa.microservices.paymentservice.dto;

import java.util.Date;
import java.util.UUID;

import com.virtusa.microservices.paymentservice.model.Transaction;

import lombok.Data;

/**
 * Response DTO given back to end-user for {@link Transaction}.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Data
public class ResponseTransactionDto {
	private UUID transactionId;

	private boolean status;

	private ResponseWalletDto sender;

	private ResponseWalletDto receiver;

	private Date time;

	private double amount;
}
