package com.librarymanagementsystem.book.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class Book {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int bookid;
	private String bookName;
	private String author;
	private String edition;
	private String genre;
	private String filePath;
	private double price;
	
	
	public Book() {
		super();
	}
	public Book(int bookid, String bookName, String author, String edition, String genre, String filePath,
			double price) {
		super();
		this.bookid = bookid;
		this.bookName = bookName;
		this.author = author;
		this.edition = edition;
		this.genre = genre;
		this.filePath = filePath;
		this.price = price;
	}
	public int getBookid() {
		return bookid;
	}
	public void setBookid(int bookid) {
		this.bookid = bookid;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Books [bookid=" + bookid + ", bookName=" + bookName + ", author=" + author + ", edition=" + edition
				+ ", genre=" + genre + ", filePath=" + filePath + ", price=" + price + "]";
	}
	
	

}
