package com.librarymanagementsystem.book.dao;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.librarymanagementsystem.book.entity.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

}
