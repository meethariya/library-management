package com.librarymanagementsystem.book.dto;

import lombok.Data;

/**
 * @author MADHUSUDANA
 * @since 30-Sep-2023
 */
@Data
public class BookResponseDto {
	private int bookid;
	private String bookName;
	private String author;
	private String edition;
	private String genre;
	private String filePath;
	private double price;

}
