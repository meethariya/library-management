package com.librarymanagementsystem.book.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.librarymanagementsystem.book.dto.BookRequestDto;
import com.librarymanagementsystem.book.dto.BookResponseDto;
import com.librarymanagementsystem.book.service.BookServiceImpl;

/**
 * @author MADHUSUDANA
 * @since 08-Oct-2023
 */

@RestController
@RequestMapping("/api/v1/books/admin/")
public class BookAdminController {

	@Autowired
	private BookServiceImpl bookService;

	/**
	 * 
	 * @param bookRequestDto
	 * @return books
	 * @throws IOException
	 */
	@PostMapping
	public ResponseEntity<BookResponseDto> addBook(@ModelAttribute BookRequestDto bookRequestDto,
			@RequestParam(name = "file") MultipartFile file) throws IOException {
		return new ResponseEntity<>(this.bookService.addBook(bookRequestDto, file), HttpStatus.CREATED);
	}

	/**
	 * 
	 * @param bookid
	 * @return ResponseEntity
	 * @throws IOException 
	 */
	@DeleteMapping("/{bookid}")
	public ResponseEntity<String> deleteBook(@PathVariable int bookid) throws IOException {
		this.bookService.deleteBook(bookid);
		return new ResponseEntity<>(bookid + " book with this id deleted", HttpStatus.OK);
	}

	/**
	 * 
	 * @param id
	 * @param request
	 * @return
	 */
	@PutMapping("/{bookId}")
	public ResponseEntity<BookResponseDto> updateBook(@PathVariable("id") int id,
			@ModelAttribute BookRequestDto request) {
		return new ResponseEntity<>(bookService.updateBook(request, id), HttpStatus.ACCEPTED);
	}

}
