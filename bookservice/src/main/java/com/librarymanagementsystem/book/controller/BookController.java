package com.librarymanagementsystem.book.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.librarymanagementsystem.book.dto.BookResponseDto;
import com.librarymanagementsystem.book.service.BookServiceImpl;

@RestController
@RequestMapping("/api/v1/books/")
public class BookController {

	@Autowired
	private BookServiceImpl bookService;

	/**
	 * 
	 * @return list of books
	 */

	@GetMapping
	public ResponseEntity<List<BookResponseDto>> getAllBooks() {
		return new ResponseEntity<>(this.bookService.getAllBooks(), HttpStatus.OK);
	}

	/**
	 * 
	 * @param bookid
	 * @return
	 */
	@GetMapping("/{bookid}")
	public ResponseEntity<BookResponseDto> getBookById(@PathVariable int bookid) {
		return new ResponseEntity<>(this.bookService.getBookById(bookid), HttpStatus.OK);
	}

}