package com.librarymanagementsystem.book.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.librarymanagementsystem.book.dao.BookRepository;
import com.librarymanagementsystem.book.dto.BookRequestDto;
import com.librarymanagementsystem.book.dto.BookResponseDto;
import com.librarymanagementsystem.book.entity.Book;
import com.librarymanagementsystem.book.exception.ResourceNotFoundException;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class BookServiceImpl implements BookService {

	@Autowired
	ModelMapper modelMapper;

	@Value("${server.port}")
	private int port;

	@Value("${spring.application.name}")
	private String appName;

	@Value("${book.resource.path}")
	private String bookRootPath;

	@Autowired
	BookRepository bookRepository;

	@Override
	public BookResponseDto addBook(BookRequestDto bookRequestDto, MultipartFile file) throws IOException {
		// Create Directory if doesn't exists
		Files.createDirectories(Paths.get(bookRootPath));

		// create file name and path
		String fileName = file.getOriginalFilename();
		if (fileName == null) {
			throw new IOException("Invalid File name");
		}
		fileName = bookRequestDto.getBookName() + "." + fileName.split("\\.")[1];
		log.info(fileName);
		String filePath = bookRootPath + fileName;

		// deletes file if exists
		Files.deleteIfExists(Paths.get(filePath));
		// saves file
		Files.copy(file.getInputStream(), Paths.get(filePath));

		bookRequestDto.setFilePath(filePath);
		return entityToResponse(bookRepository.save(requestToEntity(bookRequestDto)));

	}

	@Override
	public List<BookResponseDto> getAllBooks() {

		log.info(appName + "@" + port + ": Get all books");

		return bookRepository.findAll().stream().map(this::entityToResponse).toList();

	}

	@Override
	public void deleteBook(int bookid) throws IOException {
		log.info(appName + "@" + port + ": Delete book by id");

		// Delete book's PDF
		BookResponseDto bookById = getBookById(bookid);
		Files.deleteIfExists(Paths.get(bookById.getFilePath()));

		bookRepository.deleteById(bookid);
	}

	@Override
	public BookResponseDto getBookById(int bookid) {

		return entityToResponse(bookRepository.findById(bookid)
				.orElseThrow(() -> new ResourceNotFoundException("No book found with id: " + bookid)));
	}

	@Override
	public BookResponseDto updateBook(BookRequestDto request, int id) {
		log.info(appName + "@" + port + ": Update Book");

		// Validating user's existence
		Book book = bookRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No book found with id: " + id));

		// updating values
		book.setBookName(request.getBookName());
		book.setAuthor(request.getAuthor());
		book.setEdition(request.getEdition());
		book.setFilePath(request.getFilePath());
		book.setGenre(request.getGenre());
		book.setPrice(request.getPrice());

		return entityToResponse(bookRepository.save(book));
	}

	private Book requestToEntity(BookRequestDto bookRequestDto) {

		return modelMapper.map(bookRequestDto, Book.class);

	}

	private BookResponseDto entityToResponse(Book book) {

		return modelMapper.map(book, BookResponseDto.class);

	}

}
