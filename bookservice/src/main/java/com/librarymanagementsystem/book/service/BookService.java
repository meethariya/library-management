package com.librarymanagementsystem.book.service;


import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.librarymanagementsystem.book.dto.BookRequestDto;
import com.librarymanagementsystem.book.dto.BookResponseDto;

public interface BookService {

	public BookResponseDto addBook(BookRequestDto requestbookDto, MultipartFile file) throws IOException;
	public List<BookResponseDto> getAllBooks();
	public void deleteBook(int bookid) throws IOException;
	public BookResponseDto getBookById(int bookid);
	public BookResponseDto updateBook(BookRequestDto request, int id);

}
