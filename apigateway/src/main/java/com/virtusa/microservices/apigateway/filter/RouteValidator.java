package com.virtusa.microservices.apigateway.filter;

import java.util.List;
import java.util.function.Predicate;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

/**
 * Validates all routes.
 * 
 * @author MEETKIRTIBHAI
 * @since 04-Oct-2023
 */
@Component
public class RouteValidator {
	/**
	 * URL to be accessed by all.
	 */
	private static final List<String> permitAll = List.of("/login/oauth2/**","/api/authentication","/api/account/customer/createCustomer");

	/**
	 * URL to be accessed by admin.
	 */
	private static final List<String> admin = List.of("/api/account/admin", "/api/payment/transaction/admin", "/api/v1/books/admin/", "/api/orders/admin");

	/**
	 * Predicate to match URL with {@link RouteValidator#permitAll}.
	 */
	public static final Predicate<ServerHttpRequest> isSecured = req -> permitAll.stream()
			.noneMatch(uri -> req.getURI().getPath().contains(uri));

	/**
	 * Predicate to match URL with {@link RouteValidator#admin}
	 */
	public static final Predicate<ServerHttpRequest> isAdmin = req -> admin.stream()
			.anyMatch(uri -> req.getURI().getPath().contains(uri));
}
