package com.virtusa.microservices.apigateway.filter;

import java.security.Key;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.virtusa.microservices.apigateway.exception.UnauthorizedException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

/**
 * Authentication filter for all services.
 * 
 * @author MEETKIRTIBHAI
 * @since 04-Oct-2023
 */
@Component
public class AuthenticationFilter extends AbstractGatewayFilterFactory<AuthenticationFilter.Config> {

	/**
	 * SECRET for JWT token validation.
	 */
	private static final String SECRET = "5367566B59703373367639792F423F4528482B4D6251655468576D5A71347437";

	/**
	 * Useless yet required class for {@link AbstractGatewayFilterFactory}.
	 * 
	 * @author MEETKIRTIBHAI
	 * @since 06-Oct-2023
	 */
	public static class Config {
	}

	/**
	 * Default constructor
	 */
	public AuthenticationFilter() {
		super(Config.class);
	}

	/**
	 * Gateway filter for all services. Secures all applications.
	 */
	@Override
	public GatewayFilter apply(Config config) {
		return ((exchange, chain) -> {

			if (RouteValidator.isSecured.test(exchange.getRequest())) {
				if (!exchange.getRequest().getHeaders().containsKey(HttpHeaders.AUTHORIZATION)) {
					throw new UnauthorizedException("Missing Authorization headers");
				}

				String authHeader = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
				if (authHeader != null && authHeader.startsWith("Bearer ")) {
					authHeader = authHeader.substring(7);
				}

				validateToken(authHeader);
				Claims claims = getAllClaims(authHeader);
				// get role here
				String role = claims.get("role", String.class);
				if (RouteValidator.isAdmin.test(exchange.getRequest()) && !role.equals("admin")) {
					throw new UnauthorizedException("Unauthorized");
				}
			}

			return chain.filter(exchange);
		});

	}

	/**
	 * Validate JWT token
	 * 
	 * @param token JWT
	 */
	private void validateToken(String token) {
		Jwts.parserBuilder().setSigningKey(getSignKey()).build().parseClaimsJws(token);
	}

	/**
	 * Get Key
	 * 
	 * @return Key for JWT using SECRET
	 */
	private Key getSignKey() {
		byte[] signKey = Decoders.BASE64.decode(SECRET);
		return Keys.hmacShaKeyFor(signKey);
	}

	/**
	 * Get all claims of a token.
	 * 
	 * @param token
	 * @return Map of claims
	 */
	private Claims getAllClaims(String token) {
		return Jwts.parserBuilder().setSigningKey(getSignKey()).build().parseClaimsJws(token).getBody();
	}
}
