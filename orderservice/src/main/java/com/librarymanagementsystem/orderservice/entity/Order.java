package com.librarymanagementsystem.orderservice.entity;

import java.util.Date;
import java.util.UUID;

import org.hibernate.annotations.CurrentTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;

@Entity
@Table(name = "t_order")
@Data
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int orderId;


    private int bookId;
    private UUID transactionId;
    private int userId;
    private boolean purchased;
    private Date rentalTime;
    @CurrentTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderDate;
    
}
