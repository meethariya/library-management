package com.librarymanagementsystem.orderservice.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.librarymanagementsystem.orderservice.dto.OrderResponseDto;
import com.librarymanagementsystem.orderservice.dto.ResponseUserDto;
import com.librarymanagementsystem.orderservice.exception.InvoiceGenerationException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author MADHUSUDANA
 * @since 09-October-2023
 */

@Component
@Slf4j
public class OrderInvoiceGenerator {

	@Value("${order.invoice.path}")
	private String transactionPath;

	/**
	 * Generate invoice for the transaction.
	 * 
	 * @param transaction
	 * @param username
	 * @return path of the invoice.
	 * @throws IOException
	 * @throws InvoiceGenerationException
	 */
	public String generateOrderInvoice(OrderResponseDto order) throws IOException {

		// Create Directory if dosen't exists
		Files.createDirectories(Paths.get(transactionPath));

		String destination = transactionPath + order.getOrderId() + ".pdf";

		// creating pdf components
		Document document = new Document();
		document.addCreationDate();

		// Fonts
		Font f1 = new Font();
		f1.setStyle(Font.BOLD);
		f1.setSize(18);
		Font f2 = new Font();
		f2.setSize(12);
		ResponseUserDto user = order.getUser();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(new File(destination)));
			document.open();
			// order receipt
			document.add(createParagraph("Order Receipt\n", Element.ALIGN_CENTER, f1));
			// Library name
			document.add(createParagraph("Central Library \n", Element.ALIGN_CENTER, f1));


			// Library Address
			document.add(createParagraph("Central Library\nA2,Raja Road\nDelhi-294994", Element.ALIGN_RIGHT, f2));

			// User Details
			String userDetails = user.getName() + "\n" + user.getEmail();
			document.add(createParagraph(userDetails, Element.ALIGN_LEFT, f2));

			// Order Details
			document.add(createParagraph("Order Details\n", Element.ALIGN_CENTER, f2));

			// Order no & date
			document.add(createParagraph("Order Number: " + order.getOrderId() + "\nOrder Date: "
					+ order.getOrderDate().toString() + "\n\n\n", Element.ALIGN_LEFT, f2));

			// Table
			document.add(createOrderReceiptTable(order));

			// Order total cost
			document.add(createParagraph("\nTransaction Id: " + order.getTransaction().getTransactionId(), Element.ALIGN_LEFT, f2));
			document.add(createParagraph("Total: " + order.getTransaction().getAmount(), Element.ALIGN_RIGHT, f2));

			// Thanks Note
			document.add(createParagraph("\nHappy Learning!!!", Element.ALIGN_LEFT, f2));

			document.close();

			return destination;
		} catch (FileNotFoundException | DocumentException e) {
			log.error(e.getMessage());
			throw new InvoiceGenerationException(e.getMessage());
		}
	}

	/**
	 * Creates new Paragraph for given text.
	 * 
	 * @param text      content of paragraph
	 * @param alignment paragraph alignment
	 * @param f         font
	 * @return
	 */
	private Paragraph createParagraph(String text, int alignment, Font f) {
		Paragraph temp = new Paragraph(text);
		temp.setAlignment(alignment);
		temp.setFont(f);
		return temp;
	}

	/**
	 * Create new cell using text and font.
	 * 
	 * @param text cell content
	 * @param f    Font
	 * @return cell
	 */
	private PdfPCell createCell(String text, Font f) {
		return new PdfPCell(createParagraph(text, Element.ALIGN_CENTER, f));
	}

	/**
	 * 
	 * @param order
	 * @return pdfTable
	 */
	private PdfPTable createOrderReceiptTable(OrderResponseDto order) {
		// Table
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(Element.ALIGN_CENTER);
		// Fonts
		Font f3 = new Font();
		f3.setStyle(Font.BOLD);
		f3.setSize(12);

		Font f2 = new Font();
		f2.setSize(12);

		// heading
		// Sr no
		table.addCell(createCell("Sr. No.", f3));
		// Book Name
		table.addCell(createCell("Book Name", f3));
		// Author
		table.addCell(createCell("Author", f3));
		// Genre
		table.addCell(createCell("Genre", f3));
		// rental or purchase
		table.addCell(createCell("Rental/Purchase", f3));
		// rental period
		table.addCell(createCell("Rental Period", f3));
		// Book Price
		table.addCell(createCell("Price", f3));

		// Sr no.
		table.addCell(createCell("1", f2));
		// Book Name
		table.addCell(createCell(order.getBook().getBookName(), f2));
		// Author
		table.addCell(createCell(order.getBook().getAuthor(), f2));
		// Genre
		table.addCell(createCell(order.getBook().getGenre(), f2));
		// is purchased
		if (order.isPurchased()) {

			table.addCell(createCell("purchased", f2));
			table.addCell(createCell("NA", f2));
		} else {
			table.addCell(createCell("rented", f2));
			DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
			String date = df.format(order.getRentalTime());
			table.addCell(createCell(date, f2));
		}
		// price
		table.addCell(createCell(String.valueOf(order.getBook().getPrice()), f2));

		return table;
	}

}
