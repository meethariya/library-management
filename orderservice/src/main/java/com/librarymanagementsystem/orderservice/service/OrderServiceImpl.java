package com.librarymanagementsystem.orderservice.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.librarymanagementsystem.orderservice.dao.OrderRepo;
import com.librarymanagementsystem.orderservice.dto.BookResponseDto;
import com.librarymanagementsystem.orderservice.dto.OrderRequestDto;
import com.librarymanagementsystem.orderservice.dto.OrderResponseDto;
import com.librarymanagementsystem.orderservice.dto.RequestTransactionDto;
import com.librarymanagementsystem.orderservice.dto.ResponseTransactionDto;
import com.librarymanagementsystem.orderservice.entity.Order;
import com.librarymanagementsystem.orderservice.exception.InvalidDetailsException;
import com.librarymanagementsystem.orderservice.exception.ResourceNotFoundException;
import com.librarymanagementsystem.orderservice.openfeign.AccountServiceClient;
import com.librarymanagementsystem.orderservice.openfeign.BookServiceClient;
import com.librarymanagementsystem.orderservice.openfeign.PaymentServiceClient;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

/**
 * @author MADHUSUDANA
 * @since 03-October-2023
 */

@Service
@Transactional
@Slf4j
public class OrderServiceImpl implements OrderService {
	@Autowired
	ModelMapper modelMapper;

	@Value("${server.port}")
	private int port;

	@Value("${spring.application.name}")
	private String appName;

	@Value("${orderservice.rental.3}")
	private int threeMonth;

	@Value("${orderservice.rental.6}")
	private int sixMonth;

	@Value("${orderservice.rental.9}")
	private int nineMonth;

	@Value("${orderservice.adminId}")
	private int adminId;

	@Autowired
	OrderRepo orderRepository;

	@Autowired
	private AccountServiceClient accountServiceClient;
	@Autowired
	private BookServiceClient bookServiceClient;
	@Autowired
	private PaymentServiceClient paymentServiceClient;
	
	@Autowired
	OrderInvoiceGenerator orderInvoiceGenerator;

	@Override
	public List<OrderResponseDto> getAllOrders() {
		log.info(appName + "@" + port + ": Get all orders");

		return orderRepository.findAll().stream().map(this::entityToResponse).toList();
	}

	@Override
	public OrderResponseDto getOrderById(int orderid) {
		return entityToResponse(orderRepository.findById(orderid)
				.orElseThrow(() -> new ResourceNotFoundException("No order found with id: " + orderid)));
	}

	@Override
	public List<OrderResponseDto> getOrderByUserId(int userid) {
		return orderRepository.findByUserId(userid).stream().map(this::entityToResponse).toList();

	}

	public OrderResponseDto createOrder(OrderRequestDto request) {

		// TODO Calculate amount based on purchased or rental period of 3/6/9
		BookResponseDto book = bookServiceClient.getBook(request.getBookId());

		Order order = requestToEntity(request);
		double price;
		if (!request.isPurchased()) {
			switch (request.getRentalTime()) {
			case 3:
				price = threeMonth / 100d * book.getPrice();
				break;
			case 6:
				price = sixMonth / 100d * book.getPrice();
				break;
			case 9:
				price = nineMonth / 100d * book.getPrice();
				break;
			default:
				throw new InvalidDetailsException("Invalid time period");
			}
		} else {
			price = book.getPrice();
		}

		// Create object for RequestTransactionDto(senderUserId=userId,
		// receiverUserId=adminId, amount=Calculated price)
		RequestTransactionDto requestTransactionDto = new RequestTransactionDto();
		requestTransactionDto.setAmount(price);
		requestTransactionDto.setReceiverUserId(adminId);
		requestTransactionDto.setSenderUserId(request.getUserId());
		
		// Call Payment Service POST for create transaction --> transactionId
		ResponseTransactionDto transaction = paymentServiceClient.createTransaction(requestTransactionDto);
		// Call Payment service GET for execute transaction --> ResponseTransactionDto
		paymentServiceClient.executeTransaction(transaction.getTransactionId());
		order.setTransactionId(transaction.getTransactionId());

		// Set OrderResponseDto and return it.
		return entityToResponse(orderRepository.save(order));
	}

	public Resource generateInvoice(int orderId) throws IOException {
		OrderResponseDto order = getOrderById(orderId);

		// generate invoice
		String destination = orderInvoiceGenerator.generateOrderInvoice(order);
		Path path = Paths.get(destination);
		return new ByteArrayResource(Files.readAllBytes(path));
	}

	private OrderResponseDto entityToResponse(Order order) {
		OrderResponseDto dto = modelMapper.map(order, OrderResponseDto.class);
		dto.setUser(accountServiceClient.getUser(order.getUserId()));
		dto.setBook(bookServiceClient.getBook(order.getBookId()));
		dto.setTransaction(paymentServiceClient.getPayment(order.getTransactionId()));
		return dto;
	}

	private Order requestToEntity(OrderRequestDto orderRequestDto) {
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Order order = modelMapper.map(orderRequestDto, Order.class);
		if (!orderRequestDto.isPurchased()) {
			Calendar c = Calendar.getInstance();
			c.add(Calendar.MONTH, orderRequestDto.getRentalTime());
			order.setRentalTime(c.getTime());
		}
		return order;
	}

}
