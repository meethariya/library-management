package com.librarymanagementsystem.orderservice.service;

import java.util.List;

import com.librarymanagementsystem.orderservice.dto.OrderRequestDto;
import com.librarymanagementsystem.orderservice.dto.OrderResponseDto;

/**
 * @author MADHUSUDANA
 * @since 03-October-2023
 */
public interface OrderService {

	public List<OrderResponseDto> getAllOrders();

	public OrderResponseDto getOrderById(int orderid);

	public List<OrderResponseDto> getOrderByUserId(int userid);

	public OrderResponseDto createOrder(OrderRequestDto request);

	// public void generateInvoice(int orderId) throws IOException;

}
