package com.librarymanagementsystem.orderservice.exception;

/**
 * @author MADHUSUDANA
 * @since 09-Oct-2023
 */
public class InvoiceGenerationException extends RuntimeException {

	/**
	 * Default generated
	 */
	private static final long serialVersionUID = 5626387471157137964L;

	/**
	 * Default constructor
	 */
	public InvoiceGenerationException() {
		super();
	}

	/**
	 * Constructor with message.
	 * 
	 * @param message
	 */
	public InvoiceGenerationException(String message) {
		super(message);
	}
}
