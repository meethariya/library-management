package com.librarymanagementsystem.orderservice.exception;

/**
 * Throw this <b> Runtime Exception </b> when user's input is incorrect.
 * 
 * @author MADHUSUDANA
 * @since 09-Oct-2023
 */
public class InvalidDetailsException extends RuntimeException {

	/**
	 * Default generated.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor
	 */
	public InvalidDetailsException() {
		super();
	}

	/**
	 * Constructor with error message
	 * 
	 * @param message error message
	 */
	public InvalidDetailsException(String message) {
		super(message);
	}

}
