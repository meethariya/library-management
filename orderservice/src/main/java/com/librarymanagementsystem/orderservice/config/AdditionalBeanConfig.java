package com.librarymanagementsystem.orderservice.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration for all additional beans.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-September-2023
 */
@Configuration
public class AdditionalBeanConfig {
	/**
	 * Bean for {@link ModelMapper}.
	 * 
	 * @return new object for ModelMapper
	 */
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
