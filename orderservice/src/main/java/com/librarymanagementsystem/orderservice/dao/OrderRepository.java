package com.librarymanagementsystem.orderservice.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.librarymanagementsystem.orderservice.entity.Order;

/**
 * @author MADHUSUDANA
 * @since 03-October-2023
 */

public interface OrderRepository extends JpaRepository<Order, Integer> {

	public List<Order> findByUserId(int userid);

}
