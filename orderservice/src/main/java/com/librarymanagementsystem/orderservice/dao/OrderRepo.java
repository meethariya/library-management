package com.librarymanagementsystem.orderservice.dao;

import com.librarymanagementsystem.orderservice.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepo extends JpaRepository<Order,Integer> {
    public List<Order> findByUserId(int userId);
}
