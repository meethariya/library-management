package com.librarymanagementsystem.orderservice.dto;


import lombok.Data;

/**
 * Request DTO taken from end-user for {@link com.virtusa.microservices.paymentservice.model.Transaction}.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-September-2023
 */
@Data
public class RequestTransactionDto {

	private int senderUserId;

	private int receiverUserId;

	private boolean status;

	private double amount;
}
