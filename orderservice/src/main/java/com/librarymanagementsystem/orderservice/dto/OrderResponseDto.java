package com.librarymanagementsystem.orderservice.dto;

import java.util.Date;


import lombok.Data;

/**
 * @author MADHUSUDANA
 * @since 03-Oct-2023
 */
@Data
public class OrderResponseDto {
	private int orderId;
	private BookResponseDto book;
	private ResponseTransactionDto transaction;
	private ResponseUserDto user;
	private boolean purchased;
	private Date rentalTime;
	private Date orderDate;

}
