package com.librarymanagementsystem.orderservice.dto;

import lombok.Data;

/**
 * @author MADHUSUDANA
 * @since 03-October-2023
 */
@Data
public class OrderRequestDto {
	private int bookId;
	private int userId;
	private boolean purchased;
	private int rentalTime;

}
