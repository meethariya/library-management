package com.librarymanagementsystem.orderservice.dto;

import java.util.Date;
import java.util.UUID;



import lombok.Data;

/**
 * Response DTO given back to end-user for {@link com.virtusa.microservices.paymentservice.model.Transaction}.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Data
public class ResponseTransactionDto {
	private UUID transactionId;

	private boolean status;

	private ResponseWalletDto sender;

	private ResponseWalletDto receiver;

	private Date time;

	private double amount;
}
