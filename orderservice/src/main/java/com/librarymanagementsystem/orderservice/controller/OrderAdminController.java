package com.librarymanagementsystem.orderservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.librarymanagementsystem.orderservice.dto.OrderResponseDto;
import com.librarymanagementsystem.orderservice.service.OrderServiceImpl;

/**
 * @author MADHUSUDANA
 * @since 08-Oct-2023
 */

@RestController
@RequestMapping("/api/orders/admin")
public class OrderAdminController {

	@Autowired
	private OrderServiceImpl orderService;

	@GetMapping("/all")
	public ResponseEntity<List<OrderResponseDto>> getAllOrders() {
		return new ResponseEntity<>(this.orderService.getAllOrders(), HttpStatus.OK);
	}

}
