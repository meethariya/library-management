package com.librarymanagementsystem.orderservice.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.librarymanagementsystem.orderservice.dto.OrderRequestDto;
import com.librarymanagementsystem.orderservice.dto.OrderResponseDto;
import com.librarymanagementsystem.orderservice.service.OrderServiceImpl;

/**
 * @author MADHUSUDANA
 * @since 03-October-2023
 */

@RestController
@RequestMapping("/api/orders")
public class OrderController {
	@Autowired
	private OrderServiceImpl orderService;

	@GetMapping("/{orderid}")
	public ResponseEntity<OrderResponseDto> getOrderById(@PathVariable int orderid) {
		return new ResponseEntity<>(this.orderService.getOrderById(orderid), HttpStatus.OK);
	}

	@GetMapping("/userId/{userid}")
	public ResponseEntity<List<OrderResponseDto>> getOrderByUserId(@PathVariable int userid) {
		return new ResponseEntity<>(this.orderService.getOrderByUserId(userid), HttpStatus.OK);
	}
	@GetMapping("/generateInvoice/{orderId}")
	public ResponseEntity<Resource> generateOrderInvoice(@PathVariable("orderId")int orderId)
			throws IOException {
		Resource resource = orderService.generateInvoice(orderId);
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + "Order_Reciept_"+orderId + ".pdf");
		return ResponseEntity.ok()
							 .headers(headers)
							 .contentLength(resource.contentLength())
							 .contentType(MediaType.APPLICATION_PDF)
							 .body(resource);
	}

	@PostMapping("/createorder")
	public ResponseEntity<OrderResponseDto> createOrder(@RequestBody OrderRequestDto orderRequestDto) {
		return new ResponseEntity<>(this.orderService.createOrder(orderRequestDto), HttpStatus.OK);

	}

}
