package com.librarymanagementsystem.orderservice.openfeign;

import java.util.UUID;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.librarymanagementsystem.orderservice.dto.RequestTransactionDto;
import com.librarymanagementsystem.orderservice.dto.ResponseTransactionDto;

/**
 * Feign Client for Payment Service.
 * 
 * @author MADHUSUDANA
 * @since 06-October-2023
 */

@FeignClient(name = "paymentservice")
public interface PaymentServiceClient {

	@GetMapping("/api/payment/transaction/{transactionId}")
	public ResponseTransactionDto getPayment(@PathVariable("transactionId") UUID transactionId);

	@PostMapping(value = "/api/payment/transaction", consumes = "multipart/form-data")
	public ResponseTransactionDto createTransaction(RequestTransactionDto request);

	@GetMapping("/api/payment/transaction/execute/{transactionId}")
	public ResponseTransactionDto executeTransaction(@PathVariable("transactionId") UUID transactionId);

}
