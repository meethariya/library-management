package com.librarymanagementsystem.orderservice.openfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.librarymanagementsystem.orderservice.dto.BookResponseDto;

/**
 * Feign Client for Book Service.
 * 
 * @author MADHUSUDANA
 * @since 06-October-2023
 */

@FeignClient(name = "bookservice")
public interface BookServiceClient {
	/**
	 * Get book using id.
	 * 
	 * @param bookId used to fetch book
	 * @return BookResponseDto
	 */
	@GetMapping("/api/v1/books/{bookId}")
	public BookResponseDto getBook(@PathVariable("bookId") int bookid);

}
