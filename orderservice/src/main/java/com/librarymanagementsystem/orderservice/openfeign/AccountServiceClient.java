package com.librarymanagementsystem.orderservice.openfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.librarymanagementsystem.orderservice.dto.ResponseUserDto;

/**
 * Feign Client for Account Service.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-September-2023
 */
@FeignClient(name = "accountservice")
public interface AccountServiceClient {

	/**
	 * Get user using id.
	 * 
	 * @param userId used to fetch user
	 * @return ResponseUserDto
	 */
	@GetMapping("/api/account/customer/{id}")
	public ResponseUserDto getUser(@PathVariable("id") int userId);
}
