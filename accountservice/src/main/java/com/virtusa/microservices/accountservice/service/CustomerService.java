package com.virtusa.microservices.accountservice.service;

import com.virtusa.microservices.accountservice.dto.RequestUserDto;
import com.virtusa.microservices.accountservice.dto.ResponseUserDto;

/**
 * Service layer for Customer operations.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public interface CustomerService {
	/**
	 * Create User.
	 * 
	 * @param request requestDto for user
	 * @return responseDto for created user.
	 */
	public ResponseUserDto createUser(RequestUserDto request);

	/**
	 * Fallback method for {@link #createUser(RequestUserDto)}
	 * 
	 * @param request requestDto for user
	 * @return responseDto for null user.
	 */
	public ResponseUserDto createUserFallback(RequestUserDto request);

	/**
	 * Get user by id.
	 * 
	 * @param userId fetch using id
	 * @return responseDto for fetched user.
	 */
	public ResponseUserDto getUser(int userId);

	/**
	 * Get user by email.
	 * 
	 * @param email fetch using email.
	 * @return responseDto for fetched user.
	 */
	public ResponseUserDto getUser(String email);

	/**
	 * Update user's details.
	 * 
	 * @param request requestDto for user
	 * @param id      fetch using id
	 * @return responseDto for edited user.
	 */
	public ResponseUserDto updateUser(RequestUserDto request, int id);

	/**
	 * Delete user by id.
	 * 
	 * @param id fetch using id
	 */
	public void deleteUser(int id);

	/**
	 * Delete user by email.
	 * 
	 * @param email fetch using email.
	 */
	public void deleteUser(String email);
}
