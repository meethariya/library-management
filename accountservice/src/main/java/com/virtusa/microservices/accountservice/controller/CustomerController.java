package com.virtusa.microservices.accountservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservices.accountservice.dto.RequestUserDto;
import com.virtusa.microservices.accountservice.dto.ResponseUserDto;
import com.virtusa.microservices.accountservice.exception.InvalidDetailsException;
import com.virtusa.microservices.accountservice.exception.UserNotFoundException;
import com.virtusa.microservices.accountservice.service.CustomerService;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.extern.slf4j.Slf4j;

/**
 * Controller for Customer operations.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Slf4j
@RestController
@RequestMapping("/api/account/customer")
public class CustomerController {

	@Autowired
	private CustomerService service;

	/**
	 * Create User.
	 * 
	 * @param request requestDto for user
	 * @return responseDto for created user.
	 * @throws InvalidDetailsException
	 */
	@PostMapping("/createCustomer")
	@CircuitBreaker(name = "createUser", fallbackMethod = "createUserFallback")
	public ResponseEntity<ResponseUserDto> createUser(@ModelAttribute RequestUserDto request) {
		return new ResponseEntity<>(service.createUser(request), HttpStatus.CREATED);
	}

	/**
	 * Fallback method for {@link #createUser(RequestUserDto)}.
	 * 
	 * @param request requestDto from user
	 * @param e       Exception
	 * @return responseDto
	 */
	public ResponseEntity<ResponseUserDto> createUserFallback(RequestUserDto request, Exception e) {
		log.error(e.getMessage(), e);
		return new ResponseEntity<>(service.createUserFallback(request), HttpStatus.SERVICE_UNAVAILABLE);
	}

	/**
	 * Get user by id.
	 * 
	 * @param id fetch using id
	 * @return responseDto for fetched user.
	 * @throws UserNotFoundException
	 */
	@GetMapping("/{id}")
	public ResponseEntity<ResponseUserDto> getUser(@PathVariable("id") int id) {
		return new ResponseEntity<>(service.getUser(id), HttpStatus.OK);
	}

	/**
	 * Get user by email.
	 * 
	 * @param email fetch using email.
	 * @return responseDto for fetched user.
	 * @throws UserNotFoundException
	 */
	@GetMapping("/email/{email}")
	public ResponseEntity<ResponseUserDto> getUser(@PathVariable("email") String email) {
		return new ResponseEntity<>(service.getUser(email), HttpStatus.OK);
	}

	/**
	 * Update user's details.
	 * 
	 * @param id      fetch using id
	 * @param request requestDto for user
	 * @return responseDto for edited user.
	 * @throws UserNotFoundException
	 * @throws InvalidDetailsException
	 */
	@PutMapping("/{id}")
	public ResponseEntity<ResponseUserDto> updateUser(@PathVariable("id") int id,
			@ModelAttribute RequestUserDto request) {
		return new ResponseEntity<>(service.updateUser(request, id), HttpStatus.ACCEPTED);
	}

	/**
	 * Delete user by id.
	 * 
	 * @param id fetch using id
	 */
	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void deleteUser(@PathVariable("id") int id) {
		service.deleteUser(id);
	}

	/**
	 * Delete user by email.
	 * 
	 * @param email fetch using email.
	 * @throws UserNotFoundException
	 */
	@DeleteMapping("/email/{email}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void deleteUser(@PathVariable("email") String email) {
		service.deleteUser(email);
	}
}
