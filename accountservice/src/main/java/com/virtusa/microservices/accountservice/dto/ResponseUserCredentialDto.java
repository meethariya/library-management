package com.virtusa.microservices.accountservice.dto;

import lombok.Data;

/**
 * Response DTO to be given back to end-user.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
@Data
public class ResponseUserCredentialDto {

	private int id;
	private String email;
	private String role;
}
