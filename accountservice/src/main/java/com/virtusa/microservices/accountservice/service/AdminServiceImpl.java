package com.virtusa.microservices.accountservice.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.virtusa.microservices.accountservice.dto.ResponseUserDto;
import com.virtusa.microservices.accountservice.model.User;
import com.virtusa.microservices.accountservice.repository.UserRepository;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

/**
 * Implementation for {@link AdminService}.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Service
@Transactional
@Slf4j
public class AdminServiceImpl implements AdminService {

	@Autowired
	private ModelMapper modelMapper;

	@Value("${server.port}")
	private int port;

	@Value("${spring.application.name}")
	private String appName;

	@Autowired
	private UserRepository userRepository;

	/**
	 * Get all Users.
	 * 
	 * @return List of all Users
	 */
	@Override
	public List<ResponseUserDto> getUser() {
		log.info(appName + "@" + port + ": Get all users");

		return userRepository.findAll().stream().map(this::userToResponse).toList();
	}

	/**
	 * Delete all users.
	 */
	@Override
	public void deleteUser() {
		log.info(appName + "@" + port + ": Delete all User");

		userRepository.deleteAll();
	}

	/**
	 * Converts User entity to ResponseUserDto.
	 * 
	 * @param user User entity
	 * @return ResponseUserDto
	 */
	private ResponseUserDto userToResponse(User user) {
		return modelMapper.map(user, ResponseUserDto.class);
	}
}
