package com.virtusa.microservices.accountservice.service;

import java.util.List;

import com.virtusa.microservices.accountservice.dto.ResponseUserDto;

/**
 * Service layer for Admin operations.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public interface AdminService {

	/**
	 * Get all Users.
	 * 
	 * @return List of all Users
	 */
	public List<ResponseUserDto> getUser();

	/**
	 * Delete all users.
	 */
	public void deleteUser();
}
