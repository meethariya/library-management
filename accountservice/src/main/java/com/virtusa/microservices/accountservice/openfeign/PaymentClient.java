package com.virtusa.microservices.accountservice.openfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

import com.virtusa.microservices.accountservice.dto.RequestWalletDto;
import com.virtusa.microservices.accountservice.dto.ResponseCreateWalletDto;

/**
 * @author MEETKIRTIBHAI
 * @since 08-Oct-2023
 */
@FeignClient(name = "paymentservice")
public interface PaymentClient {

	/**
	 * Create wallet for a user.
	 * 
	 * @param request RequestWalletDto
	 * @return ResponseWalletDto
	 */
	@PostMapping(value = "/api/payment/wallet", consumes = "multipart/form-data")
	public ResponseEntity<ResponseCreateWalletDto> createWallet(RequestWalletDto request);
}
