package com.virtusa.microservices.accountservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.virtusa.microservices.accountservice.exception.InvalidDetailsException;
import com.virtusa.microservices.accountservice.exception.UserNotFoundException;

import lombok.extern.slf4j.Slf4j;

/**
 * Controller to handle all errors.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Slf4j
@RestControllerAdvice
public class ExceptionHandlerController {

	/**
	 * Handles error for {@link UserNotFoundException}.
	 * 
	 * @param e Error
	 * @return Error message with Not Found status.
	 */
	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<String> handleUserNotFoundException(UserNotFoundException e) {
		log.error(e.getMessage());
		return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
	}

	/**
	 * Handles error for {@link InvalidDetailsException }.
	 * 
	 * @param e Error
	 * @return Error message with Bad Request status.
	 */
	@ExceptionHandler(InvalidDetailsException.class)
	public ResponseEntity<String> handleInvalidDetailsException(InvalidDetailsException e) {
		log.error(e.getMessage());
		return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
	}
}
