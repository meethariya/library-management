package com.virtusa.microservices.accountservice.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

/**
 * User entity for Customer/Admin.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Entity
@Data
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String role;
	private boolean enabled;
	@Column(unique = true)
	private String email;
	private String password;
	private String name;
}
