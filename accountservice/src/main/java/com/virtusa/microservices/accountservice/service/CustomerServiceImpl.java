package com.virtusa.microservices.accountservice.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.virtusa.microservices.accountservice.dto.RequestUserCredentialDto;
import com.virtusa.microservices.accountservice.dto.RequestUserDto;
import com.virtusa.microservices.accountservice.dto.RequestWalletDto;
import com.virtusa.microservices.accountservice.dto.ResponseUserDto;
import com.virtusa.microservices.accountservice.exception.InvalidDetailsException;
import com.virtusa.microservices.accountservice.exception.UserNotFoundException;
import com.virtusa.microservices.accountservice.model.User;
import com.virtusa.microservices.accountservice.openfeign.AuthenticationClient;
import com.virtusa.microservices.accountservice.openfeign.PaymentClient;
import com.virtusa.microservices.accountservice.repository.UserRepository;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

/**
 * Implementation for {@link CustomerService}.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Service
@Transactional
@Slf4j
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private ModelMapper modelMapper;

	@Value("${server.port}")
	private int port;

	@Value("${spring.application.name}")
	private String appName;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AuthenticationClient authenticationClient;

	@Autowired
	private PaymentClient paymentClient;

	/**
	 * Create User. Also create its authentication credentials and its wallet.
	 * 
	 * @param request requestDto for user
	 * @return responseDto for created user.
	 * @throws InvalidDetailsException
	 */
	@Override
	public ResponseUserDto createUser(RequestUserDto request) {
		log.info(appName + "@" + port + ": Create User");

		// Validating for duplicate email.
		if (userRepository.findByEmail(request.getEmail()).isPresent())
			throw new InvalidDetailsException("User already exists with email: " + request.getEmail());

		// Create User
		User user = userRepository.save(requestToUser(request));

		// Create User credentials
		RequestUserCredentialDto credentialDto = modelMapper.map(user, RequestUserCredentialDto.class);
		credentialDto.setPassword(request.getPassword());
		authenticationClient.saveUser(credentialDto);

		// Create Wallet for the user
		RequestWalletDto walletDto = new RequestWalletDto();
		walletDto.setUserId(user.getId());
		paymentClient.createWallet(walletDto);

		return userToResponse(user);
	}

	/**
	 * Fallback method for {@link #createUser(RequestUserDto)}
	 * 
	 * @param request requestDto for user
	 * @return responseDto for null user.
	 */
	@Override
	public ResponseUserDto createUserFallback(RequestUserDto request) {
		// delete user
		deleteUser(request.getEmail());
		// delete user credentials
		authenticationClient.deleteUser(request.getEmail());
		return new ResponseUserDto();
	}

	/**
	 * Get user by id.
	 * 
	 * @param userId fetch using id
	 * @return responseDto for fetched user.
	 * @throws UserNotFoundException
	 */
	@Override
	public ResponseUserDto getUser(int userId) {
		log.info(appName + "@" + port + ": Get user by ID");

		User user = userRepository.findById(userId)
				.orElseThrow(() -> new UserNotFoundException("No user found with id: " + userId));
		return userToResponse(user);
	}

	/**
	 * Get user by email.
	 * 
	 * @param email fetch using email.
	 * @return responseDto for fetched user.
	 * @throws UserNotFoundException
	 */
	@Override
	public ResponseUserDto getUser(String email) {
		log.info(appName + "@" + port + ": Get user by email");

		User user = userRepository.findByEmail(email)
				.orElseThrow(() -> new UserNotFoundException("No user found with email: " + email));
		return userToResponse(user);
	}

	/**
	 * Update user's details.
	 * 
	 * @param request requestDto for user
	 * @param id      fetch using id
	 * @return responseDto for edited user.
	 * @throws UserNotFoundException
	 * @throws InvalidDetailsException
	 */
	@Override
	public ResponseUserDto updateUser(RequestUserDto request, int id) {
		log.info(appName + "@" + port + ": Update user");

		// Validating user's existence
		User user = userRepository.findById(id)
				.orElseThrow(() -> new UserNotFoundException("No user found with id: " + id));
		// Can't allow changing email.
		if (!user.getEmail().equals(request.getEmail()))
			throw new InvalidDetailsException("Email can not be modified");

		// updating values
		user.setName(request.getName());
		user.setPassword(request.getPassword());

		return userToResponse(userRepository.save(user));
	}

	/**
	 * Delete user by id.
	 * 
	 * @param id fetch using id
	 */
	@Override
	public void deleteUser(int id) {
		log.info(appName + "@" + port + ": Delete user by id");

		userRepository.deleteById(id);
	}

	/**
	 * Delete user by email.
	 * 
	 * @param email fetch using email.
	 * @throws UserNotFoundException
	 */
	@Override
	public void deleteUser(String email) {
		log.info(appName + "@" + port + ": Delete user by email");

		userRepository.deleteByEmail(email);
	}

	/**
	 * Converts RequestUserDto to User entity.
	 * 
	 * @param request requestDtoUser
	 * @return User entity object
	 */
	private User requestToUser(RequestUserDto request) {
		User user = modelMapper.map(request, User.class);
		user.setEnabled(true);
		user.setRole("ROLE_customer");
		return user;
	}

	/**
	 * Converts User entity to ResponseUserDto.
	 * 
	 * @param user User entity
	 * @return ResponseUserDto
	 */
	private ResponseUserDto userToResponse(User user) {
		return modelMapper.map(user, ResponseUserDto.class);
	}

}
