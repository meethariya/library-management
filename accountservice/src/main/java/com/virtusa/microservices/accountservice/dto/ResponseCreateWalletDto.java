package com.virtusa.microservices.accountservice.dto;

import java.util.Date;

import lombok.Data;

/**
 * Response Dto for wallet, to be used while creating wallet.
 * 
 * @author MEETKIRTIBHAI
 * @since 08-Oct-2023
 */
@Data
public class ResponseCreateWalletDto {

	private int id;

	private int userId;

	private double balance;

	private Date lastUpdated;
}
