package com.virtusa.microservices.accountservice.openfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.virtusa.microservices.accountservice.dto.RequestUserCredentialDto;
import com.virtusa.microservices.accountservice.dto.ResponseUserCredentialDto;

/**
 * @author MEETKIRTIBHAI
 * @since 08-Oct-2023
 */
@FeignClient(name = "authenticationservice")
public interface AuthenticationClient {

	/**
	 * Save UserCredentials.
	 * 
	 * @param request DTO
	 * @return ResponseDto for UserCredential
	 */
	@PostMapping(value = "/api/authentication/saveUser", consumes = "multipart/form-data")
	public ResponseEntity<ResponseUserCredentialDto> saveUser(RequestUserCredentialDto request);

	/**
	 * Delete User credentials.
	 * 
	 * @param email user's email
	 */
	@DeleteMapping(value = "/api/authentication/deleteUserByEmail/{email}")
	public void deleteUser(@PathVariable("email") String email);
}
