package com.virtusa.microservices.accountservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.virtusa.microservices.accountservice.model.User;

/**
 * Repository for {@link User} entity.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public interface UserRepository extends JpaRepository<User, Integer> {

	/**
	 * Find User by email.
	 * 
	 * @param email
	 * @return Optional for User entity
	 */
	Optional<User> findByEmail(String email);

	/**
	 * Delete user by email.
	 * 
	 * @param email user's email.
	 */
	public void deleteByEmail(String email);

}
