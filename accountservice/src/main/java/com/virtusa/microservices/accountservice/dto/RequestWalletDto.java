package com.virtusa.microservices.accountservice.dto;

import lombok.Data;

/**
 * Request DTO taken from end-user for Wallet.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Data
public class RequestWalletDto {
	private int userId;
	private double amount;
}
