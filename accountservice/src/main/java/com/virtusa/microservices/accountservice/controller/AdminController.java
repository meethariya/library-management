package com.virtusa.microservices.accountservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservices.accountservice.dto.ResponseUserDto;
import com.virtusa.microservices.accountservice.service.AdminService;

/**
 * Controller for Admin operations.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@RestController
@RequestMapping("/api/account/admin")
public class AdminController {

	@Autowired
	private AdminService service;

	/**
	 * Get all Users.
	 * 
	 * @return List of all Users
	 */
	@GetMapping
	public ResponseEntity<List<ResponseUserDto>> getUser() {
		return new ResponseEntity<>(service.getUser(), HttpStatus.OK);
	}

	/**
	 * Delete all users.
	 */
	@DeleteMapping
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void deleteUser() {
		service.deleteUser();
	}
}
