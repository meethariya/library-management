package com.virtusa.microservices.accountservice.exception;

import com.virtusa.microservices.accountservice.model.User;

/**
 * Throw this <b>RuntimeException</b> when {@link User} is not found.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
public class UserNotFoundException extends RuntimeException {

	/**
	 * Default generated
	 */
	private static final long serialVersionUID = -6594541601304958349L;

	/**
	 * Default constructor
	 */
	public UserNotFoundException() {
		super();
	}

	/**
	 * Constructor with error message.
	 * 
	 * @param message error message
	 */
	public UserNotFoundException(String message) {
		super(message);
	}

}
