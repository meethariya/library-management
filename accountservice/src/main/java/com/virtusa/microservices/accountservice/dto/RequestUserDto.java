package com.virtusa.microservices.accountservice.dto;

import lombok.Data;

/**
 * Request DTO to be accepted from end-user.
 * 
 * @author MEETKIRTIBHAI
 * @since 29-Sep-2023
 */
@Data
public class RequestUserDto {
	private String email;
	private String password;
	private String name;
}
