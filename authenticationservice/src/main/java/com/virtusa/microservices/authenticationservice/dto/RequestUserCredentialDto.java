package com.virtusa.microservices.authenticationservice.dto;

import lombok.Data;

/**
 * Request DTO to be accepted from end-user.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
@Data
public class RequestUserCredentialDto {

	private String email;
	private String password;
	private String role;
}
