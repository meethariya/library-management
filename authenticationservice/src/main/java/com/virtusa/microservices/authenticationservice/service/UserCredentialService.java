package com.virtusa.microservices.authenticationservice.service;

import org.springframework.security.oauth2.core.user.OAuth2User;

import com.virtusa.microservices.authenticationservice.dto.RequestUserCredentialDto;
import com.virtusa.microservices.authenticationservice.dto.ResponseUserCredentialDto;
import com.virtusa.microservices.authenticationservice.model.UserCredential;

/**
 * Service layer for {@link UserCredential}.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
public interface UserCredentialService {
	/**
	 * Save user.
	 * 
	 * @param request
	 * @return Response DTO for UserCredential
	 */
	public ResponseUserCredentialDto saveUser(RequestUserCredentialDto request);

	/**
	 * Generate token.
	 * 
	 * @param principal login credentials by OAuth2
	 * @return token
	 */
	public String generateToken(OAuth2User principal);

	/**
	 * Validate token.
	 * 
	 * @param token JWT token
	 */
	public void validateToken(String token);

	/**
	 * Delete user by email
	 * 
	 * @param email user's email
	 */
	public void deleteUser(String email);
}
