package com.virtusa.microservices.authenticationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.virtusa.microservices.authenticationservice.dto.RequestUserCredentialDto;
import com.virtusa.microservices.authenticationservice.dto.ResponseUserCredentialDto;
import com.virtusa.microservices.authenticationservice.exception.UserCredentialNotFoundException;
import com.virtusa.microservices.authenticationservice.service.UserCredentialService;

/**
 * Controller for Authentication Service.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
@RestController
@RequestMapping("/api/authentication")
public class AuthenticationController {

	@Autowired
	private UserCredentialService userCredentialService;

	/**
	 * Save UserCredentials.
	 * 
	 * @param request DTO
	 * @return ResponseDto for UserCredential
	 */
	@PostMapping("/saveUser")
	public ResponseEntity<ResponseUserCredentialDto> saveUser(@ModelAttribute RequestUserCredentialDto request) {
		return new ResponseEntity<>(userCredentialService.saveUser(request), HttpStatus.CREATED);
	}
	
	@DeleteMapping("/deleteUserByEmail/{email}")
	@ResponseStatus(code = HttpStatus.ACCEPTED)
	public void deleteUser(@PathVariable("email") String email) {
		userCredentialService.deleteUser(email);
	}

	/**
	 * Generate token.
	 * 
	 * @param principal authentication details
	 * @return token
	 * @throws UserCredentialNotFoundException
	 */
	@GetMapping("/generateToken")
	public ResponseEntity<String> generateToken(@AuthenticationPrincipal OAuth2User principal) {
		// validate if the user trying to generate token has valid credentials.
		return new ResponseEntity<>(userCredentialService.generateToken(principal), HttpStatus.CREATED);
	}

	/**
	 * Validate token.
	 * 
	 * @param token
	 */
	@GetMapping("/validateToken/{token}")
	@ResponseStatus(code = HttpStatus.OK)
	public void validateToken(@PathVariable("token") String token) {
		userCredentialService.validateToken(token);
	}

}
