package com.virtusa.microservices.authenticationservice.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import com.virtusa.microservices.authenticationservice.dto.RequestUserCredentialDto;
import com.virtusa.microservices.authenticationservice.dto.ResponseUserCredentialDto;
import com.virtusa.microservices.authenticationservice.exception.UserCredentialNotFoundException;
import com.virtusa.microservices.authenticationservice.model.UserCredential;
import com.virtusa.microservices.authenticationservice.repository.UserCredentialRepository;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

/**
 * Service Layer for {@link UserCredential}.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
@Slf4j
@Service
@Transactional
public class UserCredentialServiceImpl implements UserCredentialService {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserCredentialRepository userCredentialRepository;

	@Autowired
	private JwtService jwtService;

	@Value("${server.port}")
	private int port;

	@Value("${spring.application.name}")
	private String appName;

	/**
	 * Save user.
	 * 
	 * @param request
	 * @return Response DTO for UserCredential
	 */
	@Override
	public ResponseUserCredentialDto saveUser(RequestUserCredentialDto request) {
		log.info(appName + "@" + port + ": Save UserCredential");

		return entityToResponse(userCredentialRepository.save(requestToEntity(request)));
	}

	/**
	 * Generate Token.
	 * 
	 * @param principal UserCred by OAuth2
	 * @return Token
	 * @throws UserCredentialNotFoundException
	 */
	@Override
	public String generateToken(OAuth2User principal) {
		log.info(appName + "@" + port + ": Generate Token");

		String email = (String) principal.getAttribute("email");
		UserCredential userCredential = userCredentialRepository.findByEmail(email)
				.orElseThrow(() -> new UserCredentialNotFoundException("User not registered with email: "+email));

		return jwtService.generateToken(userCredential.getEmail(), userCredential.getRole());
	}

	/**
	 * Validate Token.
	 * 
	 * @param token JWT token
	 */
	@Override
	public void validateToken(String token) {
		log.info(appName + "@" + port + ": Validate Token");

		jwtService.validateToken(token);
	}

	/**
	 * Delete user by email
	 * 
	 * @param email user's email
	 */
	@Override
	public void deleteUser(String email) {
		userCredentialRepository.deleteByEmail(email);		
	}

	/**
	 * Converts RequestDto to Entity. Also hashes password.
	 * 
	 * @param request DTO
	 * @return UserCredential
	 */
	private UserCredential requestToEntity(RequestUserCredentialDto request) {
		UserCredential userCredential = modelMapper.map(request, UserCredential.class);
		userCredential.setPassword(passwordEncoder.encode(request.getPassword()));
		return userCredential;
	}

	/**
	 * Converts Entity to ResponseDto.
	 * 
	 * @param userCredential entity
	 * @return ResponseUserCredentialDto
	 */
	private ResponseUserCredentialDto entityToResponse(UserCredential userCredential) {
		return modelMapper.map(userCredential, ResponseUserCredentialDto.class);
	}

}
