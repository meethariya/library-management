package com.virtusa.microservices.authenticationservice.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Custom implementation for {@link UserDetails}.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
@NoArgsConstructor
@AllArgsConstructor
public class CustomUserDetails implements UserDetails {

	/**
	 * Default generated.
	 */
	private static final long serialVersionUID = -740532673695647057L;

	private String username;
	private String password;
	private String role;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<String> roles = new HashSet<>();
		roles.add(role);
		return roles.stream().map(SimpleGrantedAuthority::new).toList();
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	public CustomUserDetails(UserCredential u){
		this.username = u.getEmail();
		this.password = u.getPassword();
		this.role = u.getRole();
	}
}
