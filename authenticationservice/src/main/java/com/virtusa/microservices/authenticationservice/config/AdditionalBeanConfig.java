package com.virtusa.microservices.authenticationservice.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.virtusa.microservices.authenticationservice.service.CustomUserDetailsService;

import jakarta.annotation.PostConstruct;

/**
 * Configuration file for additional beans.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
@Configuration
public class AdditionalBeanConfig {

	/**
	 * Bean for {@link ModelMapper}.
	 * 
	 * @return new object for ModelMapper
	 */
	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	/**
	 * Bean for {@link BCryptPasswordEncoder}.
	 * 
	 * @return new object for PasswordEncoder
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/**
	 * Bean for {@link AuthenticationManager}.
	 * 
	 * @param config authentication configuration
	 * @return object for AuthenticationManager
	 * @throws Exception
	 */
	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
		return config.getAuthenticationManager();
	}

	/**
	 * Bean for {@link UserDetailsService}.
	 * 
	 * @return new object for CustomUserDetailsService
	 */
	@Bean
	public UserDetailsService userDetailsService() {
		return new CustomUserDetailsService();
	}
	
	@PostConstruct
	public void setSSL() {
		System.setProperty("javax.net.ssl.trustStore","NUL"); 
		System.setProperty("javax.net.ssl.trustStoreType","Windows-ROOT"); 
	}
}
