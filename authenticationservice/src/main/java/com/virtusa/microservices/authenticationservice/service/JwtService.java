package com.virtusa.microservices.authenticationservice.service;

import java.security.Key;
import java.util.Date;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

/**
 * Service Layer for JWT implementations.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
@Component
public class JwtService {

	/**
	 * JWT secret token
	 */
	public static final String SECRET = "5367566B59703373367639792F423F4528482B4D6251655468576D5A71347437";

	/**
	 * Validate Token.
	 * 
	 * @param token JWT token
	 */
	public void validateToken(String token) {
		Jwts.parserBuilder().setSigningKey(getSignKey()).build().parseClaimsJws(token);
	}

	/**
	 * Generate Token.
	 * 
	 * @param username userCred name
	 * @return Token
	 */
	public String generateToken(String username, String role) {
		return createToken(username, role);
	}

	/**
	 * Generate token that is set to expire after 30 mins.
	 * 
	 * @param claims   headers
	 * @param username userCred name
	 * @return token
	 */
	private String createToken(String username, String role) {
		return Jwts.builder().claim("role", role).setSubject(username).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 30))
				.signWith(getSignKey(), SignatureAlgorithm.HS256).compact();
	}

	/**
	 * Get key using SECRET.
	 * 
	 * @return signature key
	 */
	private Key getSignKey() {
		byte[] signKey = Decoders.BASE64.decode(SECRET);
		return Keys.hmacShaKeyFor(signKey);
	}
}
