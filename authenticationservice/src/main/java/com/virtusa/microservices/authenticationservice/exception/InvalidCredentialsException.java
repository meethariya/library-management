package com.virtusa.microservices.authenticationservice.exception;

import com.virtusa.microservices.authenticationservice.model.UserCredential;

/**
 * Throw this <b>RunTime Exception</b> when {@link UserCredential} are
 * incorrect.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
public class InvalidCredentialsException extends RuntimeException {

	/**
	 * Default generated.
	 */
	private static final long serialVersionUID = 943955115469590701L;

	/**
	 * Default Constructor
	 */
	public InvalidCredentialsException() {
		super();
	}

	/**
	 * Constructor with error message
	 * 
	 * @param message error message.
	 */
	public InvalidCredentialsException(String message) {
		super(message);
	}

}
