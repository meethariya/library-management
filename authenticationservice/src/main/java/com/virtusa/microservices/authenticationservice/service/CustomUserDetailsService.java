package com.virtusa.microservices.authenticationservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.virtusa.microservices.authenticationservice.exception.UserCredentialNotFoundException;
import com.virtusa.microservices.authenticationservice.model.CustomUserDetails;
import com.virtusa.microservices.authenticationservice.repository.UserCredentialRepository;

/**
 * Custom implementation for {@link UserDetailsService}.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
@Component
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserCredentialRepository userCredentialRepository;

	/**
	 * Get UserDetails from database by email.
	 * 
	 * @return UserDetails
	 * @throws UsernameNotFoundException
	 */
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return userCredentialRepository.findByEmail(email).map(CustomUserDetails::new)
				.orElseThrow(() -> new UserCredentialNotFoundException("No user found with email: " + email));
	}

}
