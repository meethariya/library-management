package com.virtusa.microservices.authenticationservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.virtusa.microservices.authenticationservice.model.UserCredential;

/**
 * Repository layer for {@link UserCredential}.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
public interface UserCredentialRepository extends JpaRepository<UserCredential, Integer> {

	/**
	 * Find UserCredentials by email.
	 * 
	 * @param email
	 * @return Optional for UserCredential
	 */
	public Optional<UserCredential> findByEmail(String email);

	/**
	 * Delete userCredentials by email.
	 * 
	 * @param email user's email
	 */
	public void deleteByEmail(String email);
}
