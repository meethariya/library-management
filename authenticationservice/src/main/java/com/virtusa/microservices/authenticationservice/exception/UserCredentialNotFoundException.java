package com.virtusa.microservices.authenticationservice.exception;

import com.virtusa.microservices.authenticationservice.model.UserCredential;

/**
 * Throw this <b>RunTime Exception</b> when {@link UserCredential} is not found.
 * 
 * @author MEETKIRTIBHAI
 * @since 03-Oct-2023
 */
public class UserCredentialNotFoundException extends RuntimeException {

	/**
	 * Default Generated.
	 */
	private static final long serialVersionUID = -9004422085978310256L;

	/**
	 * Default Constructor.
	 */
	public UserCredentialNotFoundException() {
		super();
	}

	/**
	 * Constructor with error message.
	 * 
	 * @param message error message
	 */
	public UserCredentialNotFoundException(String message) {
		super(message);
	}

}
